<?php

namespace Newebtime\AgencyTheme;

use Anomaly\Streams\Platform\Addon\Theme\Theme;

/**
 * Class AgencyTheme
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class AgencyTheme extends Theme
{

}
