<?php

return [
    'enabled_header' => [
        'name'    => 'Affichage du Header',
        'options' => [
            'every_page'    => 'Toutes les pages',
            'homepage_only' => 'Page d\'accueil uniquement',
            'disabled'      => 'Désactivé',
        ]
    ],
    'top_menu'       => [
        'name'         => 'Top Menu',
        'instructions' => 'Si aucun menu n\'est sélectionné, la <strong>structure()</strong> des pages sera utilisée.',
    ],
    'inline_assets'  => [
        'name'         => 'Inline Assets',
        'instructions' => 'Cela n\'est pas recommendé en HTTP/2.',
        'options'      => [
            'yes' => 'Oui',
            'no'  => 'Non',
        ],
    ],
    'top_login'      => [
        'name'         => 'Top Login',
        'instructions' => 'Afficher le Login/Logout dans le top menu ?',
        'options'      => [
            'yes' => 'Oui',
            'no'  => 'Non',
        ],
    ],
];
