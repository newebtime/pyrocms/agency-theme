<?php

use Anomaly\NavigationModule\Menu\MenuModel;

return [
    'enabled_header' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                '1' => 'newebtime.theme.agency::setting.enabled_header.options.every_page',
                '2' => 'newebtime.theme.agency::setting.enabled_header.options.homepage_only',
                '3' => 'newebtime.theme.agency::setting.enabled_header.options.disabled',
            ],
            'default_value' => '2'
        ],
    ],
    'top_menu' => [
        'type'   => 'anomaly.field_type.relationship',
        'config' => [
            'title_name' => 'name',
            'related'    => MenuModel::class,
        ],
    ],
    'inline_assets' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'yes' => 'newebtime.theme.agency::setting.inline_assets.options.yes',
                'no'  => 'newebtime.theme.agency::setting.inline_assets.options.no',
            ],
            'default_value' => 'no'
        ],
        'required' => true
    ],
    'top_login' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'yes' => 'newebtime.theme.agency::setting.top_login.options.yes',
                'no'  => 'newebtime.theme.agency::setting.top_login.options.no',
            ],
        ],
    ],
];
