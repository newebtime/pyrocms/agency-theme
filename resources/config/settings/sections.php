<?php

return [
    [
        'tabs' => [
            'general'      => [
                'title'  => 'newebtime.theme.agency::tab.general',
                'fields' => [
                    'enabled_header',
                    'inline_assets',
                ],
            ],
            'navigation' => [
                'title'  => 'newebtime.theme.agency::tab.navigation',
                'fields' => [
                    'top_menu',
                    'top_login',
                ],
            ],
        ],
    ],
];
